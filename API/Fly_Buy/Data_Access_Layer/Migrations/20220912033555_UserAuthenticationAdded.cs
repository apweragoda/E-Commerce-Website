﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data_Access_Layer.Migrations
{
    public partial class UserAuthenticationAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                    name: "Users",
                    columns: table => new
                    {
                        Id = table.Column<int>(nullable: false)
                            .Annotation("SqlServer:Identity", "1, 1"),
                        Username = table.Column<string>(nullable: true),
                        Password = table.Column<string>(nullable: false),
                        EmailAddress = table.Column<string>(nullable: false),
                        Role = table.Column<string>(nullable: true),
                        Surname = table.Column<string>(nullable: true),
                        GivenName = table.Column<string>(nullable: true)
                    });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
