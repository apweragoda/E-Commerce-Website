﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data_Access_Layer.Repository.Entities
{
    public class Shipping
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [MaxLength(12)]
        public string FirstName { get; set; }
        [MaxLength(12)]
        public string LastName { get; set; }
        public int PhoneNum { get; set; }
        [MaxLength(200)]
        public string Address { get; set; }
        public int PostalCode { get; set; }
        public string Status { get; set; }
        public DateTime ShippingDate { get; set; }
        public DateTime ExpectedDeliveryDate { get; set; }
        public DateTime DeliveryDate { get; set; }


        public int ShippingOptionId { get; set; }

        public int OrderId { get; set; }
        
    }
}
