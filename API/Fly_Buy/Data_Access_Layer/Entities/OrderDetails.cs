﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data_Access_Layer.Repository.Entities
{
    public class OrderDetails
    {
        public int Id { get; set; }

        [Display(Name = "Order")]
        [Required]
        public int OrderId { get; set; }
        [Display(Name = "Product")]
        [Required]
        public int ProductId { get; set; }
        
        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }

        [ForeignKey("ProductId")]
        public virtual Product Products { get; set; }
    }
}
