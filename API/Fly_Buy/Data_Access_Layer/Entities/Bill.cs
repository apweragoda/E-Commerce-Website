﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data_Access_Layer.Repository.Entities
{
    public class Bill
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public float Total { get; set; }
        public float Discount { get; set; }
        public string PaymentType { get; set; }
        public int OrderId { get; set; }
        public DateTime PaymentDate { get; set; }
    }
}
