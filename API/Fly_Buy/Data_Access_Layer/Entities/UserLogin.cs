﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data_Access_Layer.Entities
{
    public class UserLogin
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
