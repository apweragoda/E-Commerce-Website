﻿using Data_Access_Layer.Entities;

using System.Collections.Generic;

namespace Data_Access_Layer
{
    public interface IUserRepository
    {
        User AddUser(User user);
        User UpdateUser(User user);
        int DeleteUser(int id);
        ICollection<User> GetAllUsers();
        User GetUser(int id);
        User GetUserWithEmail(string email);
        /*void AddRefreshToken(User user, RefreshToken refreshToken);
        User GetUserByRefreshToken(string refreshToken);*/
    }
}