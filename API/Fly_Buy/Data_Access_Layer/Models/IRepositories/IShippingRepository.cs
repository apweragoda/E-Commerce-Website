﻿using Data_Access_Layer.Repository.Entities;
using System.Collections.Generic;

namespace Data_Access_Layer
{
    public interface IShippingRepository
    {
        IEnumerable<Shipping> GetAllShippingDetails();
        void AddShippingDetails(int orderId, Shipping shipping);
    }
}