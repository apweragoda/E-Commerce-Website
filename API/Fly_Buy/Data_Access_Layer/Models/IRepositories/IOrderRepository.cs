﻿using Data_Access_Layer.Repository.Entities;
using System;
using System.Collections.Generic;

namespace Data_Access_Layer
{
    public interface IOrderRepository
    {
        //IEnumerable<Order> GetAllOrders(int userId);
        Order GetOrder(int orderId);
        Order Checkout(Order anOrder);

        //Order AddOrder(Order order);
    }
}