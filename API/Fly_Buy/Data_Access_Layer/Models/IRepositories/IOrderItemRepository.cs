﻿using Data_Access_Layer.Repository.Entities;
using System.Collections.Generic;

namespace Data_Access_Layer
{
    public interface IOrderItemRepository
    {
        void AddOrderItems(int orderId, ICollection<OrderDetails> orderDetails);
        //IEnumerable<OrderDetails> GetOrderItems(int orderId);
    }
}