﻿using Data_Access_Layer.Repository.Entities;
using System.Collections.Generic;

namespace DataAccessLayer.Models
{ 
    public interface IShippingOptionsRepository
    {
        ICollection<ShippingOption> ReturnShippingOptions();
    }
}