﻿using Data_Access_Layer.Repository.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data_Access_Layer.Repository
{
    public class OrderRepository : IOrderRepository
    {
        private readonly ApplicationDbContext context;

        public OrderRepository(ApplicationDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context)); ;
        }

        public Order Checkout(Order anOrder, Product products)
        {
            context.Orders.Add(anOrder);
            context.SaveChanges();

            return anOrder;
        }


        public Order GetOrder(int orderId)
        {
            var orderEntity = context.Orders.FirstOrDefault(o => o.Id == orderId);
            return orderEntity;  
                    
        }

        public Order Checkout(Order anOrder)
        {
            throw new NotImplementedException();
        }


        /*public Order AddOrder(Order order)
        {
            context.Orders.Add(order);
            context.SaveChanges();
            
            return order;
        }*/

        /*public IEnumerable<Order> GetAllOrders(int userId)
        {
            return context.Orders
                .Include(order => order.Shipping).ThenInclude(shipping => shipping.ShippingOption)
                .Include(order => order.OrderItems).ThenInclude(item => item.Product)
                .Include(order => order.Bill)
                .Where(order => order.UserId == userId).ToList();
        }*/

    }
}


