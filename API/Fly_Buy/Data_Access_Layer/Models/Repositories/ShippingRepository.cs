﻿using Data_Access_Layer.Repository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data_Access_Layer.Repository
{
    public class ShippingRepository : IShippingRepository
    {
        private readonly ApplicationDbContext ctx;
        private readonly IOrderRepository orderRepository;

        public ShippingRepository(ApplicationDbContext ctx, IOrderRepository orderRepository)
        {
            this.ctx = ctx;
            this.orderRepository = orderRepository;
        }

        public IEnumerable<Shipping> GetAllShippingDetails()
        {
            return ctx.ShippingInfo.ToList();
        }

        public void AddShippingDetails(int orderId, Shipping shipping)
        {
            /*var order = orderRepository.GetOrder(orderId);
            order.Shipping = shipping;
            ctx.SaveChanges();*/
        }
    }
}
