﻿using Data_Access_Layer;
using Data_Access_Layer.Repository.Entities;
using System.Collections.Generic;
using System.Linq;

namespace DataAccessLayer.Models.Repositories
{
    public class ShippingOptionsRepository : IShippingOptionsRepository
    {
        private readonly ApplicationDbContext context;

        public ShippingOptionsRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public ICollection<ShippingOption> ReturnShippingOptions()
        {
            return context.ShippingOption.ToList();
        }
    }
}
