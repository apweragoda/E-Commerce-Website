﻿using Data_Access_Layer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Data_Access_Layer
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDbContext ctx;

        public UserRepository(ApplicationDbContext ctx)
        {
            this.ctx = ctx;
        }
        public ICollection<User> GetAllUsers()
        {
            return ctx.Users.ToList();
        }
        public User GetUser(int id)
        {
            return ctx.Users
                .FirstOrDefault(o => o.Id == id);
        }

        public User GetUserWithEmail(string email)
        {
            return ctx.Users.FirstOrDefault(u => u.EmailAddress == email);
        }

        public User AddUser(User user)
        {
            if (user == null)
                Console.WriteLine("Error occured");

            ctx.Users.Add(user);
            ctx.SaveChanges();
            return user;

        }


        public User UpdateUser(User user)
        {
            if (user == null)
                return null;

            ctx.Users.Update(user);
            ctx.SaveChanges();
            return user;

        }

        public int DeleteUser(int id)
        {
            var user = ctx.Users
                        .FirstOrDefault(u => u.Id == id);
            if (user != null)
            {
                ctx.Users.Remove(user);
                Console.WriteLine("User deleted - ", user.EmailAddress);
                return ctx.SaveChanges();
            }
            return 0;
        }


        /*public void AddRefreshToken(User user, RefreshToken refreshToken)
        {
            if (user == null)
            {
                Console.WriteLine("Error occured");
            }
            //user.RefreshToken.Add(refreshToken);
            ctx.Users.Update(user);
            ctx.SaveChanges();
        }

        public User GetUserByRefreshToken(string refreshToken)
        {
            return ctx.Users.SingleOrDefault(u => u.RefreshToken.Any(t => t.Token == refreshToken));
        }*/

    }
}
