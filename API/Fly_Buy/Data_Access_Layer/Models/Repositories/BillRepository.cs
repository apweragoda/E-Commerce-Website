﻿using Data_Access_Layer.Repository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data_Access_Layer.Repository
{
    public class BillRepository : IBillRepository
    {
        private readonly ApplicationDbContext ctx;

        public BillRepository(ApplicationDbContext ctx)
        {
            this.ctx = ctx;
        }

        public IEnumerable<Bill> GetAllBillDetails()
        {
            return ctx.Bills.ToList();
        }

        public Bill AddBillDetails(Bill bill)
        {
            ctx.Bills.Add(bill);
            ctx.SaveChanges();
            return bill;
        }
    }
}
