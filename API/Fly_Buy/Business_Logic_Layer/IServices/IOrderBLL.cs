﻿using Business_Logic_Layer.Models;
using System;
using System.Collections.Generic;

namespace Business_Logic_Layer.Services
{
    public interface IOrderBLL
    {
        //IEnumerable<OrderModel> GetAllOrders(int userId, int userIdFromToken);
        //OrderModel GetOrder(int orderId);
        //OrderModel AddOrder(OrderCreationModel order);
        OrderModel Checkout(OrderModel anOrder);
    }
}