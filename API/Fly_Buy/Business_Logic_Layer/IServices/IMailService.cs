﻿using Business_Logic_Layer.Models;
using System.Threading.Tasks;

namespace Business_Logic_Layer.Services
{
    public interface IMailService
    {
        Task SendOrderEmailAsync(OrderModel order);
    }
}