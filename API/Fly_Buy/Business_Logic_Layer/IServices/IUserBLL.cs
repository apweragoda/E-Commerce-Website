﻿using Business_Logic_Layer.Models;
using System.Collections.Generic;

namespace Business_Logic_Layer
{
    public interface IUserBLL
    {
        UserModel AddUser(UserCreationModel user);
        int DeleteUser(int id);
        ICollection<UserModel> GetAllUsers();
        UserModel GetUser(int id);
        UserModel Login(UserLoginModel loginModel);
        UserModel UpdateUser(int id, UserCreationModel user);
        UserModel GetUserWithEmail(string email);

        //UserModel RefreshExpiredJWTtoken(string refreshTokenToRenew);
        //string GenerateToken(UserModel userModel);
        //RefreshToken CreateRefreshToken();
        //UserModel GenerateUserWithRefreshToken(User user);
    }
}