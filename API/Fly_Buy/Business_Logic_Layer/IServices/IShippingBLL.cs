﻿using Business_Logic_Layer.Models;
using Data_Access_Layer.Repository.Entities;
using System.Collections.Generic;

namespace Business_Logic_Layer.Services
{
    public interface IShippingBLL
    {
        IEnumerable<ShippingOption> GetShippingOptions();
    }
}