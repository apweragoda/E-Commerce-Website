﻿using Business_Logic_Layer.Models;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;


namespace Business_Logic_Layer.Services
{
    public class MailService : IMailService
    {
        private readonly MailSettings mailSettings;
        public MailService(IOptions<MailSettings> mailSettings)
        {
            this.mailSettings = mailSettings.Value;
        }

        public async Task SendOrderEmailAsync(OrderModel order)
        {

            var mailBody = @$"<!DOCTYPE html>
                <html>
                <head>
                    <meta charset = ""utf-8""/>
                     title></title>
                </head>
                <body>

                 <div style = ""max-width: 60vw; margin: 0 auto"" >
 
                    <h1 style = ""text-align:center; color:#333"" > Trends </ h1 >
                     <p style=""font-size: 16px""><span style = ""font-weight:bold""> Hi {order.Name} , </span> your order is Confirmed !</ p >
             
                     <p style = ""font-size:16px"">
                          Keep shopping and earn more discounts! 
                          You can see your summary of your recent purchase <a href = ""http://localhost:4200/order/{order.Id}"" > here </a>.
              
                          Please allow up to 2 business days(excluding weekends and public holidays) to process and ship your order.
                      </p>
                  </div>
                </body>
                </html>
              ";


            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(mailSettings.Mail));
            email.To.Add(MailboxAddress.Parse(order.Email));
            email.Subject = "Order Confirmation.";

            var builder = new BodyBuilder();
            builder.HtmlBody = mailBody;
            email.Body = builder.ToMessageBody();

            using var smtp = new SmtpClient();
            smtp.Connect(mailSettings.Host, mailSettings.Port, SecureSocketOptions.StartTls);
            smtp.Authenticate(mailSettings.Mail, mailSettings.Password);
            await smtp.SendAsync(email);

            smtp.Disconnect(true);
        }
    }
}
