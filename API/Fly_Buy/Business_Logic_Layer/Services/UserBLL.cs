﻿using System;
using AutoMapper;
using Business_Logic_Layer.Models;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using UserModel = Business_Logic_Layer.Models.UserModel;
using Data_Access_Layer.Entities;
using Data_Access_Layer;

namespace Business_Logic_Layer
{
    public class UserBLL : IUserBLL
    {
        private readonly AppSettings appSettings;
        private readonly IUserRepository userRepository;
        private readonly IMapper userMapper;
        private readonly ILogger logger;


        public UserBLL()
        {

        }
        public UserBLL(IOptions<AppSettings> appSettings, IUserRepository userRepository, IMapper mapper, ILogger<UserBLL> logger)
        {
            this.appSettings = appSettings.Value ?? throw new ArgumentNullException(nameof(appSettings));
            this.userRepository = userRepository;
            this.userMapper = mapper;
            this.logger = logger;
        }

        public UserModel Login(UserLoginModel loginModel)
        {
            var customer = userRepository.GetUserWithEmail(loginModel.Email);

            if (customer == null)
            {
                return null;
            }

            bool isValidPassword = BCrypt.Net.BCrypt.Verify(loginModel.Password, customer.Password);

            if (!isValidPassword)
            {
                logger.LogWarning("Password incorrect");
                return null;
            }
            var result = userMapper.Map<UserModel>(customer);

            //var userModel = GenerateUserWithRefreshToken(customer);

            return result;
        }

        public ICollection<UserModel> GetAllUsers()
        {
            var user = userRepository.GetAllUsers();

            var result = userMapper.Map<ICollection<UserModel>>(user);

            logger.LogWarning("Displaying all user details");

            return result;
        }

        public UserModel GetUser(int userId)
        {
            var user = userRepository.GetUser(userId);

            var result = userMapper.Map<UserModel>(user);

            logger.LogWarning("User: " + user.Username + " searched");

            return result;
        }

        public UserModel AddUser(UserCreationModel userCreationModel)
        {
            var userExist = userRepository.GetUserWithEmail(userCreationModel.Email);
            if (userExist != null)
            {
                return null;
            }
            var userEntity = new User()
            {
                Username = userCreationModel.UserName,
                EmailAddress = userCreationModel.Email,
                Password = BCrypt.Net.BCrypt.HashPassword(userCreationModel.Password)
            };

            var changes = userRepository.AddUser(userEntity);

            var result = userMapper.Map<UserModel>(changes);

            //var newUser = GenerateUserWithRefreshToken(changes);

            logger.LogWarning("User: " + userEntity.Username + " added");

            return result;
        }

        public UserModel UpdateUser(int id, UserCreationModel user)
        {
            var existingUser = userRepository.GetUser(id);

            if (existingUser != null)
            {
                existingUser.Username = user.UserName;
                existingUser.EmailAddress = user.Email;
                existingUser.Password = user.Password;
                var userEntity = userMapper.Map<User>(existingUser);
                var changes = userRepository.UpdateUser(userEntity);
                var userModel = userMapper.Map<UserModel>(changes);
                logger.LogWarning("User: " + user.UserName + " updated");
                return userModel;
            }
            logger.LogWarning("User not found");
            return null;

        }

        public int DeleteUser(int id)
        {
            var changes = userRepository.DeleteUser(id);
            logger.LogWarning("User: " + id + " deleted");
            return (int)changes;
        }

        public UserModel GetUserWithEmail(string email)
        {
            logger.LogWarning("Getting user by email");

            var verified = userRepository.GetUserWithEmail(email);

            if (verified == null)
            {
                logger.LogError("Invalid input");
                return null;
            }

            var userModel = userMapper.Map<UserModel>(verified);

            logger.LogWarning("Getting user details by email - " + userModel.Username);

            return userModel;
        }
        /*public UserModel GenerateUserWithRefreshToken(UserModel user)
        {
            var userModel = new UserModel();
            var model = userMapper.Map<UserModel>(user);

            userModel.Id = user.Id;
            userModel.IsAuthenticated = true;
            userModel.Token = GenerateToken(model);
            userModel.Email = user.Email;
            userModel.Username = user.Username;

            var refreshToken = CreateRefreshToken();

            userModel.RefreshToken = refreshToken.Token;

            userModel.RefreshTokenExpiration = refreshToken.Expires;

            //var model = userMapper.Map<User>(userModel);

            userRepository.AddRefreshToken(user, refreshToken);

            return userModel;
        }

        public RefreshToken CreateRefreshToken()
        {
            var randomNumber = new byte[32];

            using var generator = new RNGCryptoServiceProvider();

            generator.GetBytes(randomNumber);

            return new RefreshToken
            {
                Token = Convert.ToBase64String(randomNumber),
                Expires = DateTime.UtcNow.AddDays(10),
                Created = DateTime.UtcNow
            };
        }*/


        /*public string GenerateToken(UserModel userModel)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            var key = Encoding.ASCII.GetBytes(appSettings.JWTkey);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new System.Security.Claims.ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, userModel.Id.ToString()),
                }),
                Expires = DateTime.UtcNow.AddMinutes(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }

        public UserModel RefreshExpiredJWTtoken(string refreshTokenToRenew)
        {
            var userModel = new UserModel();

            var user = userRepository.GetUserByRefreshToken(refreshTokenToRenew);

            if (user == null)
            {
                userModel.IsAuthenticated = false;
                userModel.Message = "No user with the token found";
                return userModel;
            }

            var refreshToken = user.RefreshToken.Single(r => r.Token == refreshTokenToRenew);

            if (!refreshToken.IsActive)
            {
                userModel.IsAuthenticated = false;
                userModel.Message = "Token is not active";
                return userModel;
            }

            refreshToken.Revoked = DateTime.Now;

            var newRefreshToken = CreateRefreshToken();

            userRepository.AddRefreshToken(user, newRefreshToken);

            var model = userMapper.Map<UserModel>(user);

            userModel.IsAuthenticated = true;

            userModel.Token = GenerateToken(model);

            userModel.Email = user.Email;

            userModel.Id = user.Id;

            userModel.UserName = user.UserName;

            userModel.RefreshToken = newRefreshToken.Token;

            userModel.RefreshTokenExpiration = refreshToken.Expires;

            return userModel;

        }*/
    }
}
