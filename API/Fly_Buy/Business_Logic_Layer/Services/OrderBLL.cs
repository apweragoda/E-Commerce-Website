﻿using AutoMapper;
using Business_Logic_Layer.Models;
using Data_Access_Layer;
using Data_Access_Layer.Repository;
using Data_Access_Layer.Repository.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business_Logic_Layer.Services
{
    public class OrderBLL : IOrderBLL
    {
        private readonly IMailService mailService;
        private readonly IOrderRepository orderRepository;
        private readonly IUserRepository userRepository;
        private readonly IMapper orderMapper;

        public OrderBLL(IOrderRepository orderRepository, IMailService mailService, IOrderRepository repository, IUserRepository userRepository, IMapper mapper)
        {
            this.mailService = mailService;
            this.userRepository = userRepository;
            this.orderRepository = repository;
            this.orderMapper = mapper;
        }

        

        public OrderModel Checkout(OrderModel anOrder)
        {
            var orderEntity = orderMapper.Map<Order>(anOrder);

            var orderModel = orderRepository.Checkout(orderEntity);
            
            var orderDetails = orderMapper.Map<OrderModel>(orderModel);

            return orderDetails;

        }



        public OrderModel GetOrder(int orderId)
        {
            var orderEntity = orderRepository.GetOrder(orderId);

            if (orderEntity == null)
            {
                return null;
            }

            return orderMapper.Map<OrderModel>(orderEntity);
        }


        /*public OrderModel AddOrder(OrderCreationModel orderCreationModel)
        {
            Order orderDetails = orderMapper.Map<Order>(orderCreationModel);

            var createdOrder = orderRepository.AddOrder(orderDetails);

            var createdOrderModel = orderMapper.Map<OrderModel>(createdOrder);

            mailService.SendOrderEmailAsync(createdOrderModel);
            return createdOrderModel;
        }*/

        /*public IEnumerable<OrderModel> GetAllOrders(int userId, int userIdFromToken)
        {
            if (userId != userIdFromToken)
            {
                return null;
            }

            var orders = orderRepository.GetAllOrders(userId);

            var result = orderMapper.Map <IEnumerable<OrderModel>>(orders) ;

            return result;
        }*/




    }
}
