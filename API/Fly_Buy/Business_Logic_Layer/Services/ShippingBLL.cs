﻿
using Data_Access_Layer.Repository.Entities;
using DataAccessLayer.Models;
using System.Collections.Generic;

namespace Business_Logic_Layer.Services
{
    public class ShippingBLL : IShippingBLL
    {
        private readonly IShippingOptionsRepository shippingOptionsRepository;

        public ShippingBLL(IShippingOptionsRepository shippingOptionsRepository)
        {
            this.shippingOptionsRepository = shippingOptionsRepository;
        }
        public IEnumerable<ShippingOption> GetShippingOptions()
        {
            var shipping = shippingOptionsRepository.ReturnShippingOptions();
            return shipping;
        }

    }
}
