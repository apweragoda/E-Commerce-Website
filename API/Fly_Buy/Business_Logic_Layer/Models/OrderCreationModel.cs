﻿using Data_Access_Layer.Repository.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Business_Logic_Layer.Models
{
    public class OrderCreationModel
    {
        [Required]
        public int OrderNo { get; set; }
        [Required]
        public string PhoneNo { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public DateTime OrderDate { get; set; }
        public List<OrderDetailsModel> OrderDetails { get; set; }
        public List<ShippingModel> Shipping { get; set; }
        public List<BillModel> Bill { get; set; }

    }
}
