﻿using Data_Access_Layer.Repository.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Business_Logic_Layer.Models
{
    public class OrderItemCreationModel
    {

        public ProductModel ProductId { get; set; }
        public int Quantity { get; set; }
        public string Size { get; set; }

    }
}
