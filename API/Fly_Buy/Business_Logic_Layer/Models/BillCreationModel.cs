﻿using Data_Access_Layer.Repository.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Business_Logic_Layer.Models
{
    public class BillCreationModel
    {
        
        public float Total { get; set; }

        public string PaymentType { get; set; }
 
    }
}
