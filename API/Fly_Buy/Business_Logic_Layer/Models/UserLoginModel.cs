﻿using System.ComponentModel.DataAnnotations;

namespace Business_Logic_Layer.Models
{
    public class UserLoginModel
    {
        public string Username { get; set; }
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password)] 
        public string Password { get; set; }
    }
}
