﻿using Data_Access_Layer.Repository.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Business_Logic_Layer.Models
{
    public class OrderModel
    {
        public OrderModel()
        {
            OrderDetails = new List<OrderDetailsModel>();
        }
        public int Id { get; set; }
        public string OrderNo { get; set; }
        public string Name { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }

        public DateTime OrderDate { get; set; }

        public virtual List<OrderDetailsModel> OrderDetails { get; set; }

        public BillModel Bill { get; set; }



    }
}
