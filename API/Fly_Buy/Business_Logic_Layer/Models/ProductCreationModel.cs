﻿using System.ComponentModel.DataAnnotations;

namespace Business_Logic_Layer.Models
{
    public class ProductCreationModel
    {
        [Required]
        public int Id { get; set; }
        [MaxLength(200)] 
        public string Name { get; set; }
        [Required]
        public decimal Price { get; set; }
        public string Image { get; set; }
        public string ProductColor { get; set; }
        [Required]
        public bool IsAvailable { get; set; }
        [Required]
        public int ProductCategoryId { get; set; }
    }
}
