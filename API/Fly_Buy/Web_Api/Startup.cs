using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Data_Access_Layer;
using Data_Access_Layer.Repository;
using Business_Logic_Layer;
using Serilog;
using Business_Logic_Layer.Models;
using Microsoft.AspNetCore.Http;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Identity;
using Data_Access_Layer.Repository.Entities;
using System.Linq;
using Business_Logic_Layer.Services;
using System.Reflection;
using DataAccessLayer.Models.Repositories;
using DataAccessLayer.Models;

namespace Web_Api
{
    public class Startup
    {
        private readonly IConfiguration configuration;
        private readonly string defaultPolicy = "default";

        private static int id1;
        private static int id2;

        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;

        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(30);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            services.AddCors((setup) =>
            {
                setup.AddPolicy(defaultPolicy, (options) =>
                {
                    options.WithOrigins("http://localhost:5000", "http://localhost:4200").AllowAnyHeader().AllowAnyMethod().AllowCredentials();
                });
            });

            services.AddMvc(options => options.EnableEndpointRouting = false);

            var appSettingsSection = configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            var appSettings = appSettingsSection.Get<AppSettings>();

            var connectionString = configuration.GetConnectionString("DefaultConnection");

            services.AddDbContext<ApplicationDbContext>(opt => opt.UseSqlServer(connectionString));

            /*services.AddIdentity<Data_Access_Layer.Repository.Entities.ApplicationUser, IdentityRole>(options =>
            {
                options.User.RequireUniqueEmail = false;
            })
            .AddEntityFrameworkStores<ApplicationDbContext>()
            .AddDefaultTokenProviders();*/
            

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "FLYBUY Online Shopping Site", Version = "v1" });
            });



            var jwtKey = Encoding.ASCII.GetBytes(appSettings.JWTkey);

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = configuration["Jwt:Issuer"],
                        ValidAudience = configuration["Jwt:Audience"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"]))
                    };
                });

            services.AddSingleton<AppSettings>();
            services.AddTransient<IMailService, MailService>();

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IOrderItemRepository, OrderItemRepository>();
            services.AddScoped<IShippingRepository, ShippingRepository>();
            services.AddScoped<IShippingOptionsRepository, ShippingOptionsRepository>();
            services.AddScoped<IBillRepository, BillRepository>();

            services.AddControllers().AddNewtonsoftJson(cfg => cfg.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            RegisterService.ConfigureServices(services);
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddScoped<IProductBLL, ProductBLL>();
            services.AddScoped<IUserBLL, UserBLL>();
            services.AddScoped<IOrderBLL, OrderBLL>();
            services.AddScoped<IBillBLL, BillBLL>();
            services.AddScoped<IShippingBLL, ShippingBLL>();


            // Email settings
            var mailSetting = configuration.GetSection("MailSettings");
            services.Configure<MailSettings>(mailSetting);
            var appSettin = mailSetting.Get<MailSettings>();
            //services.AddTransient<IMailService, MailService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Online Shopping Site v1"));
            }
            else
            {
                app.UseExceptionHandler("/api/error");
            }

            app.UseCors(defaultPolicy);

            app.UseMvc();

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseCookiePolicy();

            app.UseSession();

            app.UseAuthentication();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            var scope = app.ApplicationServices.CreateScope();
            var context = scope.ServiceProvider.GetService<ApplicationDbContext>();

            var cat = context.ProductCategories.FirstOrDefault(c => c.ProductCategory == "Womens");
            var prod = context.Products.FirstOrDefault(c => c.Name == "Blouse");

            if (cat == null)
            {
                SeedCategoriesData(context);
                if (prod == null)
                    SeedProductData(context);
            }
        }
        public void RemoveCategoriesData(ApplicationDbContext db)
        {
            var category1 = db.ProductCategories.FirstOrDefault(c => c.ProductCategory == "Womens"); ;
            var category2 = db.ProductCategories.FirstOrDefault(c => c.ProductCategory == "Mens");

            db.ProductCategories.Remove(category1);
            db.ProductCategories.Remove(category2);
            db.SaveChanges();

            SeedCategoriesData(db);
        }

        public void SeedCategoriesData(ApplicationDbContext db)
        {
            ProductCategories category3 = new ProductCategories
            {
                ProductCategory = "Womens"
            };

            db.ProductCategories.Add(category3);
            db.SaveChanges();
            id1 = category3.Id;
            var wom = db.ProductCategories.FirstOrDefault(c => c.ProductCategory == "Womens");
            id1 = wom.Id;

            ProductCategories category4 = new ProductCategories
            {
                ProductCategory = "Mens"
            };

            db.ProductCategories.Add(category4);
            db.SaveChanges();
            id2 = category4.Id;
            var men = db.ProductCategories.FirstOrDefault(c => c.ProductCategory == "Mens");
            id2 = men.Id;
        }

        public void RemoveProductData(ApplicationDbContext db)
        {
            Product product1 = db.Products.FirstOrDefault(c => c.Name == "Blouse");
            Product product2 = db.Products.FirstOrDefault(c => c.Name == "Short Sleeve Shirt");
            Product product3 = db.Products.FirstOrDefault(c => c.Name == "Dress");
            Product product4 = db.Products.FirstOrDefault(c => c.Name == "Long Sleeve Shirt");

            db.Products.Remove(product1);
            db.Products.Remove(product2);
            db.Products.Remove(product3);
            db.Products.Remove(product4);
            
            db.SaveChanges();

            SeedProductData(db);
        }


        public void SeedProductData(ApplicationDbContext db)
        {
            Product productA = new Product
            {
                Name = "Blouse",
                Price = 140,
                Image = "assets/img/product/product-1.jpg",
                ProductColor = "Yellow",
                IsAvailable = true,
                ProductCategoryId = id1
            };
            Product productB = new Product
            {
                Name = "Short Sleeve Shirt",
                Price = 100,
                Image = "assets/img/product/product-2.jpg",
                ProductColor = "Pink and White",
                IsAvailable = true,
                ProductCategoryId = id2
            };
            Product productC = new Product
            {
                Name = "Dress",
                Price = 120,
                Image = "assets/img/product/product-3.jpg",
                ProductColor = "Orange",
                IsAvailable = true,
                ProductCategoryId = id1
            };

            //ProductCreationModel product4 = new ProductCreationModel

            Product productD = new Product

            {
                Name = "Long Sleeve Shirt",
                Price = 150,
                Image = "assets/img/product/product-4.jpg",
                ProductColor = "Light Pink",
                IsAvailable = true,
                ProductCategoryId = id2
            };

            db.Products.Add(productA);
            db.Products.Add(productB);
            db.Products.Add(productC);
            db.Products.Add(productD);

            db.SaveChanges();
        }

    }
}