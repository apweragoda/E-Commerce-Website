﻿using Business_Logic_Layer;
using Business_Logic_Layer.Models;
using Data_Access_Layer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Web_Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private IConfiguration config;
        private readonly IUserBLL userBLL;

        public LoginController(IConfiguration config, IUserBLL userBLL)
        {
            this.config = config;
            this.userBLL = userBLL;
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult Login([FromBody] UserLoginModel userLogin)
        {
            UserModel user = (UserModel)Authenticate(userLogin);

            if (user != null)
            {
                var token = Generate(user);
                user.Token = token;
                return Ok(user);
            }

            return NotFound("User not found");
        }

        private string Generate(UserModel user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Username),
                new Claim(ClaimTypes.Email, user.EmailAddress),
                new Claim(ClaimTypes.GivenName, user.GivenName),
                new Claim(ClaimTypes.Surname, user.Surname),
                new Claim(ClaimTypes.Role, user.Role)
            };

            var token = new JwtSecurityToken(config["Jwt:Issuer"],
              config["Jwt:Audience"],
              claims,
              expires: DateTime.Now.AddMinutes(15),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private object Authenticate(UserLoginModel userLogin)
        {
            //var currentUser = UserConstantsModel.Users.FirstOrDefault(o => o.EmailAddress.ToLower() == userLogin.Email.ToLower() && o.Password == userLogin.Password);
            var currentUser = userBLL.Login(userLogin);
            if (currentUser != null)
            {

                return currentUser;
            }

            return null;
        }
    }
}
