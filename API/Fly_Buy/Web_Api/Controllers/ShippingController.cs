﻿using Business_Logic_Layer.Models;
using Business_Logic_Layer.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web_Api.Controllers
{
    [Route("api/[Controller]")]
    public class ShippingController : Controller
    {
        private readonly IShippingBLL shippingBLL;
        private readonly ILogger<ShippingController> logger;

        public ShippingController(IShippingBLL shippingBLL, ILogger<ShippingController> logger)
        {
            this.shippingBLL = shippingBLL;
            this.logger = logger;
        }

        [HttpGet]
        public ActionResult GetShippingOptions()
        {
            var options = shippingBLL.GetShippingOptions();
            return Ok(options);
        }
    }
}
