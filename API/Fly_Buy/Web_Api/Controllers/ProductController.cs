﻿using Business_Logic_Layer;
using Data_Access_Layer;
using Data_Access_Layer.Repository.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Web_Api.Utility;


namespace Web_Api.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly ApplicationDbContext db;
        private readonly IProductBLL productBLL;
        private readonly ILogger logger;
        private readonly IHostingEnvironment he;

        public ProductController(ApplicationDbContext db, IProductBLL productBLL, ILogger<ProductController> logger, IHostingEnvironment he)
        {
            this.db = db;
            this.productBLL = productBLL;
            this.logger = logger;
            this.he = he;
        }
        // GET: api/<ProductController>
        [HttpGet]
        public IActionResult GetAllProducts()
        {
            return Ok(db.Products.ToList());
        }

        [HttpGet]
        [Route("{id}")]
        [ActionName("GetSingleProduct")]
        public IActionResult GetSingleProduct(int id)
        {
            //var product = db.Products.FirstOrDefault(p => p.Id == id);
            var product = productBLL.GetSingleProduct(id);

            if (product == null)
            {
                logger.LogWarning("Product not found");
                return NotFound("Product not found");
            }

            return Ok(product);

        }

        [HttpGet]
        [Route("Category")]
        [ActionName("GetProductsByCategory")]
        public IActionResult GetProductsByCategory(int category)
        {

            var products = productBLL.GetProductsByCategory(category);
            return Ok(products);
        }

        [HttpPost]
        [Route("Details")]
        [ActionName("Details")]
        public ActionResult ProductDetails(int? id)
        {
            List<Product> products = new List<Product>();
            if (id == null)
            {
                return NotFound();
            }

            var product = db.Products.FirstOrDefault(c => c.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            products = HttpContext.Session.Get<List<Product>>("products");
            if (products == null)
            {
                products = new List<Product>();
            }
            products.Add(product);
            HttpContext.Session.Set("products", products);
            return RedirectToAction(nameof(Index));
        }


        [HttpPost]
        [Route("Remove")]
        public IActionResult Remove(int? id)
        {
            List<Product> products = HttpContext.Session.Get<List<Product>>("products");
            if (products != null)
            {
                var product = products.FirstOrDefault(c => c.Id == id);
                if (product != null)
                {
                    products.Remove(product);
                    HttpContext.Session.Set("products", products);
                }
            }
            return RedirectToAction(nameof(Index));
        }


        [HttpGet]
        [Route("Cart")]
        public IActionResult Cart()
        {
            List<Product> products = HttpContext.Session.Get<List<Product>>("products");
            if (products == null)
            {
                products = new List<Product>();
            }
            return Ok(products);
        }


        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromForm]Product product, IFormFile image)
        {
            var searchProduct = db.Products.FirstOrDefault(c => c.Name == product.Name);
            if (searchProduct != null)
            {
                logger.LogInformation("This product is already exist");
                return Ok(searchProduct);
            }

            if (image != null)
            {
                var name = Path.Combine("assets/img/product", Path.GetFileName(image.FileName));
                await image.CopyToAsync(new FileStream(name, FileMode.Create));
                product.Image = "assets/img/product/" + image.FileName;
                    
                logger.LogInformation(he.WebRootPath);
            }

            if (image == null)
            {
                product.Image = "assets/img/product/no-image.PNG";
            }
            db.Products.Add(product);
            await db.SaveChangesAsync();

            return Ok(product);
        }

        [HttpPost]
        [Route("Edit")]
        public async Task<IActionResult> Edit([FromForm] Product products, IFormFile image)
        {
            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    var name = Path.Combine("assets/img/product", Path.GetFileName(image.FileName));
                    await image.CopyToAsync(new FileStream(name, FileMode.Create));
                    products.Image = "assets/img/product/" + image.FileName;
                    logger.LogInformation(he.WebRootPath);

                }

                if (image == null)
                {
                    products.Image = "assets/img/product/no-image.PNG";
                    logger.LogInformation(he.WebRootPath);

                }
                db.Products.Update(products);
                await db.SaveChangesAsync();
                return Ok(products);
            }

            return Ok(products);
        }


        [HttpDelete]
        [Route("Delete")]
        [ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirm([FromForm] int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = db.Products.FirstOrDefault(c => c.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            db.Products.Remove(product);
            await db.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
