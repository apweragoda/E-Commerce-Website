﻿using Business_Logic_Layer.Models;
using Business_Logic_Layer.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web_Api.Controllers
{
    [Route("api/[Controller]")]
    public class BillController : Controller
    {
        private readonly IBillBLL billBLL;
        private readonly ILogger<BillController> logger;

        public BillController(IBillBLL billBLL, ILogger<BillController> logger)
        {
            this.billBLL = billBLL;
            this.logger = logger;
        }

        [HttpGet]
        public IEnumerable<BillModel> GetAllBillDetails()
        {
            return billBLL.GetAllBillDetails();
        }

        [HttpPost]
        public ActionResult<BillModel> AddBill([FromBody] BillCreationModel bill)
        {
            try
            {
                logger.LogWarning("New bill created");
                return billBLL.AddBillDetails(bill);
            }
            catch (Exception ex)
            {
                logger.LogError($"Failed to create bill : {ex}");

                return BadRequest("Failed to create bill");
            }

        }
    }
}
