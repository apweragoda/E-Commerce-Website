﻿using AutoMapper;
using Business_Logic_Layer.Models;
using Business_Logic_Layer.Services;
using Data_Access_Layer;
using Data_Access_Layer.Repository.Entities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web_Api.Utility;

namespace Web_Api.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[Controller]")]
    [ApiController]
    public class OrderController : Controller
    {
        private readonly ApplicationDbContext db;
        private readonly IOrderBLL orderBLL;
        private readonly IBillBLL billBLL;
        private readonly ILogger<OrderController> logger;
        private readonly IMapper mapper;

        public OrderController(ApplicationDbContext db, IOrderBLL orderBLL, IBillBLL billBLL, ILogger<OrderController> logger, IMapper mapper)
        {
            this.db = db;
            this.orderBLL = orderBLL;
            this.billBLL = billBLL;
            this.logger = logger;
            this.mapper = mapper;
        }

        [HttpPost]
        [Route("Checkout")]
        [ValidateAntiForgeryToken]
        public IActionResult Checkout([FromBody]OrderModel anOrder)
        {
            List<Product> products = HttpContext.Session.Get<List<Product>>("products");

            if (products != null)
            {
                foreach (var product in products)
                {
                    OrderDetailsModel orderDetails = new OrderDetailsModel();
                    orderDetails.OrderId = anOrder.Id;
                    orderDetails.ProductId = product.Id;
                    anOrder.OrderDetails.Add(orderDetails);
                }
            }
            else
            {
                Console.WriteLine("No products in the cart");
            }

            anOrder.OrderNo = "GetOrderNo()";
            var billEntity = mapper.Map<BillCreationModel>(anOrder.Bill);
            billBLL.AddBillDetails(billEntity);

            var order = orderBLL.Checkout(anOrder);

            HttpContext.Session.Set("products", new List<Product>());
            return Ok(order);
        }

        [HttpGet]
        [Route("GetOrderNo")]
        public string GetOrderNo()
        {
            int rowCount = db.Orders.ToList().Count() + 1;
            return rowCount.ToString("000");
        }

        
    }
}
